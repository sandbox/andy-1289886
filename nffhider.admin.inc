<?php
/**
 * @file
 * Provides admin settings for nodeform fieldsets hider.
 */

/**
 * Implements hook_form_FORM_ID_alter for node_type_form via wrapper function.
 *
 * @see nffhider_form_node_type_form_alter()
 */
function _nffhider_form_node_type_form_alter(&$form, &$form_state) {
  $type = $form['#node_type']->type;
  $fieldsets = _nffhider_get_fieldsets($type);
  $settings = _nffhider_load_settings($type);
  
  $default_value = array_keys($fieldsets);
  $default_value = array_diff($default_value, $settings);
  
  $form['nffhider'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fieldset settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['nffhider']['nffhider_fieldsets'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Active fieldsets'),
    '#options' => $fieldsets,
    '#description' => t('If you want to hide a fieldset you can deselect it here.'),
    '#default_value' => $default_value,
  );
  array_unshift($form['#submit'], 'nffhider_node_type_form_submit');
}

/** 
 * Submit handler for node_type_form.
 */
function nffhider_node_type_form_submit($form, &$form_state) {
  
  // We store the fieldsets we want to hide, but the checkboxes represent the
  // fieldsets we want visible (more intuitive imho).
  $vals = $form_state['values']['nffhider_fieldsets'];
  // Get those that *weren't* selected.
  $vals = array_filter($vals, create_function('$a', 'return $a === 0;'));
  $vals = array_keys($vals);
  
  _nffhider_save_settings($vals, $form_state['values']['type']);
  
  // Make sure that node_type_form_submit() doesn't *also* save our values.
  unset($form_state['values']['nffhider_fieldsets']);
}

/**
 * Recursive function to find all the fieldsets in the specified content type's
 * nodeform. If there's more than one fieldset with the same name then this
 * function won't distinguish between them.
 */
function _nffhider_get_fieldsets($type, $form = NULL, &$fieldsets = array()) {

  if (!$form) {
    module_load_include('inc', 'node', 'node.pages');
    
    // Create dummy arrays, set flag in form_state to prevent us hiding the
    // fieldsets.
    $nf_form_state = array('nffhider' => TRUE);
    $node = array('uid' => 1, 'name' => '', 'type' => $type, 'language' => '');
    
    // Retrieve and prepare the nodeform.
    $form_id = $type . '_node_form';
    $form = drupal_retrieve_form($form_id, $nf_form_state, $node);
    drupal_prepare_form($form_id, $form, $nf_form_state);
    $first = TRUE;
  }
  
  foreach (element_children($form) as $child) {
    if (isset($form[$child]['#type']) && ($form[$child]['#type'] == 'fieldset')) {
      $fieldsets[$child] = $form[$child]['#title'];
    }
    
    _nffhider_get_fieldsets($type, $form[$child], $fieldsets);
  }
  
  if (isset($first)) {
    // When we've finished recursing through the form make sure there's no
    // duplicates (which could be caused by multiple fieldsets with the same
    // name).
    $fieldsets = array_unique($fieldsets);
    asort($fieldsets);
  }
  
  return $fieldsets;
}

function _nffhider_save_settings($vals, $type) {
  variable_set('nffhider_' . $type, $vals);
}
