Nodeform fieldsets hider
************************

This module lets you hide fieldsets that you don't want to be available on the
node create/edit form, configurable by content type. For example you could use
it to hide the URL path settings fieldset for content type Story.

Versions
********

It's for Drupal 6 only.

Installation & Use
******************

1. Install as normal.
2. Go to Content management -> Content types -> Edit [content type]
3. Under Fieldset settings there are checkboxes for the different fieldsets
   available on the nodeform for that content type. By default they're all
   active. Deselect those you don't want to be shown.
4. Click Save content type.
5. If you want any roles to be able to see hidden fieldsets, then give them the
   see hidden fieldsets permission. 

Notes
*****

The module doesn't alter permissions, only which fieldsets are usable from the
node create/edit form. It's possible that users will still be able to accomplish
some of the actions via other admin screens. (For example, if you hide the Menu
settings fieldset, a user with administer menu would still be able to change the
weight of a node's menu item from the menu admin screens.)

Exportability
*************

You can use strongarm to export variables starting with nffhider.


[1] http://drupal.org/documentation/install/modules-themes/modules-5-6

author: AndyF
http://drupal.org/user/220112
